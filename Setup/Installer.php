<?php
/**
 * Copyright © 2015 ShopGo. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace ShopGo\CatalogSampleData\Setup;

use Magento\Framework\Setup;

class Installer implements Setup\SampleData\InstallerInterface
{
    /**
     * Setup class for category
     *
     * @var \ShopGo\CatalogSampleData\Model\Category
     */
    protected $categorySetup;

    /**
     * Setup class for product attributes
     *
     * @var \ShopGo\CatalogSampleData\Model\Attribute
     */
    protected $attributeSetup;

    /**
     * Setup class for products
     *
     * @var \ShopGo\CatalogSampleData\Model\Product
     */
    protected $productSetup;

    /**
     * @param \ShopGo\CatalogSampleData\Model\Category $categorySetup
     * @param \ShopGo\CatalogSampleData\Model\Attribute $attributeSetup
     * @param \ShopGo\CatalogSampleData\Model\Product $productSetup
     */
    public function __construct(
        \ShopGo\CatalogSampleData\Model\Category $categorySetup,
        \ShopGo\CatalogSampleData\Model\Attribute $attributeSetup,
        \ShopGo\CatalogSampleData\Model\Product $productSetup
    ) {
        $this->categorySetup = $categorySetup;
        $this->attributeSetup = $attributeSetup;
        $this->productSetup = $productSetup;
    }

    /**
     * {@inheritdoc}
     */
    public function install()
    {
        $this->attributeSetup->install(['ShopGo_CatalogSampleData::fixtures/attributes.csv']);
        $this->categorySetup->install(['ShopGo_CatalogSampleData::fixtures/categories.csv']);
        $this->productSetup->install(
            [
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/products_gear_bags.csv',
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/products_gear_fitness_equipment.csv',
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/products_gear_fitness_equipment_ball.csv',
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/products_gear_fitness_equipment_strap.csv',
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/products_gear_watches.csv',
            ],
            [
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/images_gear_bags.csv',
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/images_gear_fitness_equipment.csv',
                'ShopGo_CatalogSampleData::fixtures/SimpleProduct/images_gear_watches.csv',
            ]
        );
    }
}
